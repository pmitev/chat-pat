//
//  ViewController.swift
//  Chat-Pat
//
//  Created by p.mitev on 16.12.16.
//  Copyright © 2016 Petar Mitev. All rights reserved.
//

import UIKit
import AVFoundation

class CameraVC: AVCamCameraViewController,CameraVCProtocolDelegate {

    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var previewView: AVCamPreviewView!
    override func viewDidLoad() {
       
        delegate = self
      _previewView = previewView
        
        super.viewDidLoad()
    
      
        
    }

   
    @IBAction func recordBtnPressed(_ sender: Any) {
        
        toggleMovieRecording()
    }
    
    @IBAction func changeCameraBtn(_ sender: Any) {
        
        changeCamera()
    }
    
    func enableCameraUI(_ enable: Bool) {
        
        cameraBtn.isEnabled = enable
        print("Camera fnctnlty works properly \(enable)")
    }
    
    func enableRecordUI(_ enable: Bool) {
        
        recordBtn.isEnabled = enable
        print("in the name of the record \(enable)")
    }
    
    func beforeRecording() {
    print("It's about to record ")
    }
    
    func recordingHasStarted() {
        print("Iiihaaa recording has started!!!")
    }

}


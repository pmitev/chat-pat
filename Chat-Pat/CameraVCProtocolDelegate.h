//
//  CameraVCProtocolDelegate.h
//  Chat-Pat
//
//  Created by p.mitev on 20.12.16.
//  Copyright © 2016 Petar Mitev. All rights reserved.
//
#import <Foundation/Foundation.h>
#ifndef CameraVCProtocolDelegate_h
#define CameraVCProtocolDelegate_h

@protocol CameraVCProtocolDelegate <NSObject>

 - (void)enableRecordUI:(BOOL)enable;
 - (void)enableCameraUI:(BOOL)enable;
- (void)beforeRecording;
- (void)recordingHasStarted;
@end

#endif /* CameraVCProtocolDelegate_h */
